<?php
/**
 * @file
 * Hooks related to the Translation Management (TMGMT) module.
 */

/**
 * Implements hook_tmgmt_source_suggestions().
 */
function ckentity_tmgmt_source_suggestions(array $items, TMGMTJob $job) {
  $suggestions = array();

  foreach ($items as $item) {
    if (($item instanceof TMGMTJobItem) && ($item->plugin == 'entity') || ($item->plugin == 'node')) {
      // Load the entity and extract the bundle name to get all fields from the
      // current entity.
      $entity = entity_load_single($item->item_type, $item->item_id);
      list(, , $bundle) = entity_extract_ids($item->item_type, $entity);
      $field_instances = field_info_instances($item->item_type, $bundle);

      // Get all translatable entity types.
      $entity_types = array_filter(variable_get('entity_translation_entity_types', array()));

      // Loop over all fields, and look for translatable text fields with text
      // processing.
      foreach ($field_instances as $instance) {
        if (empty($instance['settings']['text_processing'])) {
          continue;
        }

        $field = field_info_field($instance['field_name']);
        $field_name = $field['field_name'];

        $field_items = field_get_items($item->item_type, $entity, $field_name);
        if (empty($field_items)) {
          continue;
        }

        foreach ($field_items as $field_item) {
          if (empty($field_item['value'])) {
            continue;
          }
          // Quick check to avoid extra processing in the majority of cases.
          $text = $field_item['value'];
          if (strpos($text, '<drupal-entity') === FALSE) {
            continue;
          }

          // If still here, do the regex to get all of the embedded entities.
          if (preg_match_all('/<drupal-entity[^>]+>.*?<\/drupal-entity>/s', $text, $match)) {

            foreach ($match[0] as $element) {

              $data = ckentity_get_data_from_element($element);
              if (!empty($data)) {

                if (isset($entity_types[$data['entity_type']])) {
                  // Add all referenced entities as suggestion.
                  $ref_entity = entity_load_single($data['entity_type'], $data['entity_id']);

                  // Check if there is already a translation available for this
                  // entity. If so, just continue with the next one.
                  $handler = entity_translation_get_handler($data['entity_type'], $ref_entity);
                  if ($handler instanceof EntityTranslationHandlerInterface) {
                    $translations = $handler->getTranslations();
                    if (isset($translations->data[$job->target_language])) {
                      continue;
                    }
                  }

                  // Add suggestion.
                  $suggestions[] = array(
                    'job_item' => tmgmt_job_item_create('entity', $data['entity_type'], $data['entity_id']),
                    'reason' => t('Entity embedded in field @label', array('@label' => $instance['label'])),
                    'from_item' => $item->tjiid,
                  );
                }
              }
            }
          }
        }
      }
    }
  }

  return $suggestions;
}