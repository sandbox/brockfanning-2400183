<?php
/**
 * @file
 * Helper functions for CKEntity.
 */

/**
 * Helper function to get all enabled buttons in json-ready form.
 */
function ckentity_get_button_data() {

  ctools_include('export');
  $buttons = ctools_export_crud_load_all('ckentity_button');
  $icons = ckentity_get_icon_options(FALSE);

  $button_data = array();
  foreach ($buttons as $button) {
    $data = unserialize($button->data);

    // Use the specified icon if we know the path.
    if (!empty($data['image']) && !empty($icons[$data['image']])) {
      $image = $icons[$data['image']];
    }
    // Otherwise use a generic image.
    else {
      $image = base_path() . drupal_get_path('module', 'ckentity') . '/ckeditor/ckentity/icons/drupalentity.png';
    }

    // Grab and filter the optional help text, if any.
    $help = '';
    if (!empty($data['help']['value'])) {

      $help = check_markup($data['help']['value'], $data['help']['format'],
        $GLOBALS['language']->language);
    }

    // Because of the way Drupal constructs the Drupal.settings array, we can't
    // have numeric indexes. So we need to convert numeric indexes into strings,
    // which currently happens for the formatters array.
    $string_formatters = array();
    foreach ($data['entity_config']['formatters'] as $key => $formatter) {
      $string_formatters['formatter' . $key] = $formatter;
    }
    $data['entity_config']['formatters'] = $string_formatters;

    $entity_type = $data['entity_type'];
    $entity_info = entity_get_info($entity_type);
    $create_links = array();
    if ($data['create_links']) {

      // Unfortunately there is nothing in entity_get_info to tell us the
      // path for creating an entity. So we try to figure it out.

      // Node and Taxonomy depend on bundles
      if ('node' == $entity_type || 'taxonomy_term' == $entity_type) {
        $bundles = (empty($data['entity_config']['bundles'])) ? array() :
          $data['entity_config']['bundles'];
        foreach (array_filter($bundles) as $bundle) {
          $label = t('Create @bundle', array('@bundle' => $entity_info['bundles'][$bundle]['label']));
          if ('node' == $entity_type) {
            $path = 'node/add/' . $bundle;
            $create_links[$path] = $label;
          }
          elseif ('taxonomy_term' == $entity_type) {
            $path = 'admin/structure/taxonomy/' . $bundle . '/add';
            $create_links[$path] = $label;
          }
        }
      }
      // Also support users.
      elseif ('user' == $entity_type) {
        $path = 'admin/people/create';
        $create_links[$path] = t('Create');
      }
      elseif ('file' == $entity_type) {
        $path = 'file/add';
        $create_links[$path] = t('Create');
      }
    }

    $edit_link = FALSE;
    if ($data['edit_link']) {

      // Unfortunately there is nothing in entity_get_info to tell us the
      // path for editing an entity. But we assume [type]/[id]/edit, with
      // some exceptions.
      $edit_link = $entity_type . '/[id]/edit';
      if ('taxonomy_term' == $entity_type) {
        $edit_link = 'taxonomy/term/[id]/edit';
      }
    }

    $button_data[$button->name] = array(
      'name' => $button->name,
      'label' => $data['label'],
      'image' => $image,
      'entityType' => $data['entity_type'],
      'formatters' => $data['entity_config']['formatters'],
      'help' => $help,
      'browser' => ($data['entity_config']['entity_selection'] != 'autocomplete'),
      'browserInfo' => $data['entity_config']['entity_selection'],
      'createLinks' => (empty($create_links)) ? FALSE : $create_links,
      'editLink' => $edit_link,
      'alignment' => (empty($data['alignment'])) ? FALSE : $data['alignment'],
    );
  }
  return $button_data;
}

/**
 * Get the possible field types that an entity might be displayed inside of.
 *
 * @param string $entity_type
 *   The type of entity that might be displayed.
 *
 * @return
 *   An array of field types that might display the passed entity type.
 */
function ckentity_get_entity_field_types($entity_type) {

  $field_types = array();

  // All entity types, by definition, can be displayed as entityreference.
  $field_types[] = 'entityreference';

  // Support for nodes.
  if ('node' == $entity_type) {

    $field_types[] = 'node_reference';
  }
  // Support for users.
  if ('user' == $entity_type) {

    $field_types[] = 'user_reference';
  }
  // Support for taxonomy.
  if ('taxonomy_term' == $entity_type) {

    $field_types[] = 'taxonomy_term_reference';
  }
  // Support for files.
  if ('file' == $entity_type) {

    $field_types[] = 'image';
    $field_types[] = 'file';
  }

  // Support for any other entity_types.
  $additional_field_types = module_invoke_all('ckentity_field_types', $entity_type);
  $field_types = array_merge($field_types, $additional_field_types);

  return $field_types;
}

/**
 * Helper function to get all the field formatters that we can use to display
 * entity types.
 */
function ckentity_get_entity_field_formatters($entity_type) {

  $formatters = field_info_formatter_types();
  $field_types = ckentity_get_entity_field_types($entity_type);
  $entity_formatters = array();
  foreach ($formatters as $formatter => $info) {

    foreach ($info['field types'] as $field_type) {

      if (in_array($field_type, $field_types)) {

        $entity_formatters[$formatter] = $info;
        break;
      }
    }
  }
  return $entity_formatters;
}


/**
 * Helper function to get an option list of icon options.
 *
 * $support_jipi bool
 *   Whether to return special form API data if the jipi module is enabled.
 */
function ckentity_get_icon_options($support_jipi = TRUE) {

  $path = base_path() . drupal_get_path('module', 'ckentity') . '/ckeditor/ckentity/icons/';
  // Add the general "E" icon.
  $icons = array('ckentity' => $path . 'drupalentity.png');
  // Add another "E" icon for each letter of the alphabet.
  foreach (range('a', 'z') as $letter) {

    $icons['ckentity_' . $letter] = $path . 'ckentity_' . $letter . '.png';
  }

  // Allow other modules to declare some icons.
  $contrib_icons = module_invoke_all('ckentity_icons');
  $icons = array_merge($icons, $contrib_icons);

  $options = array();
  // If the jipi module is enabled (image-picker jQuery library)
  // then format the options to take advantage of it.
  if ($support_jipi && module_exists('jipi')) {
    foreach ($icons as $key => $path) {
      $options[$key] = array(
        '#value' => $key,
        '#data-img-src' => $path,
      );
    }
  }
  // Otherwise plan on using a normal select list.
  else {
    $options = $icons;
  }

  return $options;
}

/**
 * Helper function to convert our custom HTML element into structured data.
 *
 * @return array | bool
 *   On success an array containing the following elements:
 *     'entity_type'
 *     'entity_id'
 *     'entity_display'
 *     'entity_settings'
 *     'align'
 *   On failure return FALSE;
 */
function ckentity_get_data_from_element($element) {

  try {
    $xml = new SimpleXMLElement($element);
  }
  catch (Exception $e) {
    return FALSE;
  }

  // Safety mechanism.
  if ($xml->getName() != 'drupal-entity') {

    return FALSE;
  }

  $entity_type = (string)$xml->attributes()->{'data-entity-type'};
  $entity_id = (string)$xml->attributes()->{'data-entity-id'};
  $entity_display = (string)$xml->attributes()->{'data-entity-embed-display'};
  $entity_settings = (string)$xml->attributes()->{'data-entity-embed-settings'};
  $align = (string)$xml->attributes()->{'data-align'};

  if (!empty($entity_settings)) {
    $entity_settings = drupal_json_decode(html_entity_decode($entity_settings));
  }

  // We have to parse our "entity_id" because it actually contains
  // the title.
  $entity_id_pos = strrpos($entity_id, '-');
  if ($entity_id_pos !== FALSE) {

    $entity_id = trim(substr($entity_id, $entity_id_pos + 1));
  }

  // Safety net for mangled markup.
  if (!is_numeric($entity_id)) {

    return FALSE;
  }

  // If we are missing any of the required items, return nothing.
  if (empty($entity_type) || empty($entity_id) || empty($entity_display)) {

    return FALSE;
  }

  return array(
    'entity_type' => $entity_type,
    'entity_id' => $entity_id,
    'entity_display' => $entity_display,
    'entity_settings' => $entity_settings,
    'align' => $align,
  );
}

/**
 * Helper function to convert our custom HTML element into rendered entity.
 */
function ckentity_convert_element_to_entity($element, $langcode = NULL) {

  $data = ckentity_get_data_from_element($element);
  if (empty($data)) {

    return '';
  }

  if (empty($langcode)) {

    $langcode = $GLOBALS['language']->language;
  }

  $formatter_parts = explode(':', $data['entity_display']);
  $formatter_module = $formatter_parts[0];
  $formatter = $formatter_parts[1];
  $formatter_view_func = $formatter_module . '_field_formatter_view';

  if (function_exists($formatter_view_func)) {

    // Hack our way into the hook_field_formatter_view function.
    // Dummy params where needed, others from helper functions.
    $dummy_entity_type = 'node';
    $dummy_entity = new stdClass();
    $field = ckentity_field_info_field($data['entity_type'], $formatter_module);
    $instance = ckentity_field_info_instance($formatter, $data['entity_settings']);
    $items = ckentity_field_get_items($data['entity_type'], $data['entity_id'], $formatter_module);
    $display = $instance['display']['default'];
    $render = $formatter_view_func($dummy_entity_type, $dummy_entity, $field, $instance, $langcode, $items, $display);

    // Add alignment if needed.
    if (!empty($data['align'])) {
      $render['#prefix'] = '<div class="ckentity-data-align-' . $data['align'] . '">';
      $render['#suffix'] = '</div>';
      $render['#attached']['css'][] = array(
        'type' => 'file',
        'data' => drupal_get_path('module', 'ckentity') .
          '/css/ckentity.embedded.theme.css',
      );
    }

    if (!empty($render)) {

      return drupal_render($render);
    }
  }

  return '';
}
