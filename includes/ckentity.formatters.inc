<?php
/**
 * @file
 * Helper functions for Entity Embed involving field formatters.
 *
 * All of these functions are by definition hacky, because we're trying to use
 * a couple of hook in ways they were not intended to be used: displaying
 * arbitrary entities. The hooks are:
 * - hook_field_formatter_settings_form()
 * - hook_field_formatter_view()
 */

/**
 * Our own field-less and entity-less version of field_get_items().
 *
 * @param string $entity_type
 *   The entity type of the entity we are trying to embed.
 *
 * @param int $entity_id
 *   The entity id of the entity we are trying to embed.
 *
 * @param string $module
 *   The module that implements the field formatter we are going to use.
 *
 * In our version of field_get_items() we don't care at all about the field
 * or the entity that it is attached to. All we want is the output of the
 * formatter. So the point of this is return an $items array in the format
 * expected by the passed module's particular implementation of
 * hook_field_formatter_view().
 *
 * Ideally this would be a simple hook letting other modules do the work, but
 * we will hard-code support for the major players here.
 */
function ckentity_field_get_items($entity_type, $entity_id, $module) {

  // Whatever module is involved, we're almost definitely going to need the
  // entity loaded and an access check.
  $entity = entity_load_single($entity_type, $entity_id);
  $access = entity_access('view', $entity_type, $entity);

  $item = array(
    'access' => $access,
  );

  // Support the entityreference module.
  if ('entityreference' == $module) {

    $item += array(
      'entity' => $entity,
      'target_id' => $entity_id,
    );
  }

  // Support the taxonomy module (core).
  elseif ('taxonomy' == $module) {

    $item += array(
      'tid' => $entity_id,
      'name' => entity_label($entity_type, $entity_id),
      'taxonomy_term' => $entity,
    );
  }

  // Support the file  and image modules (core).
  elseif ('file' == $module || 'image' == $module) {

    // The file and image modules just expect an array version of the object.
    $item += (array) $entity;
  }

  // Support the user_reference (references) module.
  elseif ('user_reference' == $module) {

    $item += array(
      'uid' => $entity_id,
      'user' => $entity,
    );
  }

  // Support the node_reference (references) module.
  elseif ('node_reference' == $module) {

    $item += array(
      'nid' => $entity_id,
      'node' => $entity,
    );
  }

  // Support the file_entity module.
  elseif ('file_entity' == $module) {

    // In general the file_entity module expects items to be
    // file objects, but in array form, similar to core image/file.
    $item += (array) $entity;
  }

  // If we didn't find anything, give other modules a chance.
  else {

    $contrib_items = module_invoke_all('ckentity_field_get_items', $entity_type, $entity_id, $module);
    // Make sure we only get 1 item.
    if (!empty($contrib_items)) {
      $item += array_pop($contrib_items);
    }
  }

  return array($item);
}

/**
 * Our own field-less entity-less version of field_get_display().
 *
 * @param string $formatter
 *   The entity type of the entity we are trying to embed.
 *
 * @param int $settings
 *   Optional array of settings to pass in. If omitted, the formatter's default
 *   settings will be returned.
 *
 * @return
 *   An array containing 'type' and 'settings'.
 *
 * In our version of field_get_display() we don't care at all about the field
 * or the entity/bundle that it is attached to. All we want is a display
 * array that will help us hack our way through hook_field_formatter_view() and
 * hook_field_formatter_settings_form(). This mainly means making sure the
 * 'settings' and 'type' elements are there and populated appropriately.
 *
 * Luckily this can be the same regardless of formatter/module, so we don't
 * need to hard-code module support or invoke any hooks.
 */
function ckentity_field_get_display($formatter, $settings = array()) {

  // If settings is empty, we need to get defaults.
  if (empty($settings)) {

    $formatter_info = field_info_formatter_types($formatter);
    if (!empty($formatter_info['settings'])) {
      $settings = $formatter_info['settings'];
    }
  }

  return array(
    'type' => $formatter,
    'settings' => $settings,
  );
}

/**
 * Our own field-less version of field_info_field().
 *
 * @param string $entity_type
 *   The entity type of the entity we are trying to embed.
 *
 * @param string $module
 *   The module that implements the field formatter we are going to use.
 *
 * In our version of field_info_field() we don't care at all about the field
 * itself. All we want is the field info array in a format that lets us hack
 * into hook_field_formatter_view() and hook_field_formatter_settings_form().
 * The problem is that each module may expect something different in this array.
 *
 * Ideally this would be a simple hook letting other modules do the work, but
 * we will hard-code support for the major players here.
 */
function ckentity_field_info_field($entity_type, $module) {

  $field = array();

  // Support the entityreference module.
  if ('entityreference' == $module) {

    $field['settings'] = field_info_field_settings('entityreference');
    $field['settings']['target_type'] = $entity_type;
  }

  // Support the user_reference (references) module.
  elseif ('user_reference' == $module) {

    $field['settings'] = field_info_field_settings('user_reference');
    $field['field_name'] = 'foo';
  }

  // Support node_reference (references) module.
  elseif ('node_reference' == $module) {

    $field['settings'] = field_info_field_settings('node_reference');
    $field['field_name'] = 'foo';
  }

  // Support taxonomy (core) module.
  elseif ('taxonomy' == $module) {

    $field['settings'] = field_info_field_settings('taxonomy_term_reference');
  }

  // Support file (core) module.
  elseif ('file' == $module) {

    $field['settings'] = field_info_field_settings('file');
  }

  // Support file (core) module.
  elseif ('image' == $module) {

    $field['settings'] = field_info_field_settings('image');
  }

  // If we didn't find anything, give other modules a chance.
  else {

    $field = module_invoke_all('ckentity_field_info_field', $entity_type, $module);
  }

  return $field;
}

/**
 * Our own field-less entity-less version of field_info_instance().
 *
 * @param string $formatter
 *   The entity type of the entity we are trying to embed.
 *
 * @param int $settings
 *   Optional array of settings to pass in. If omitted, the formatter's default
 *   settings will be returned.
 *
 * In our version of field_info_instance() we don't care at all about the field
 * itself. All we want is the field instance array in a format that lets us hack
 * into hook_field_formatter_view() and hook_field_formatter_settings_form().
 * The problem is that each module may expect something different in this array.
 *
 * This is mainly a wrapper around ckentity_field_get_display().
 */
function ckentity_field_info_instance($formatter, $settings = array()) {

  return array(
    'display' => array(
      'default' => ckentity_field_get_display($formatter, $settings),
    ),
  );
}


