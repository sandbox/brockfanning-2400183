<?php
/**
 * @file
 * Page callbacks for ckentity_menu().
 */

/**
 * Return a rendered entity for use as a preview in Ckeditor.
 */
function ckentity_preview() {

  $element = $_GET['value'];
  $rendered = ckentity_convert_element_to_entity($element);

  $commands = array();
  $commands[] = array(
    'command' => 'ckentityInsert',
    'html' => $rendered,
  );
  print ajax_render($commands);
  drupal_exit();
}

/**
 * Return autocomplete matches for entity selectors.
 *
 * Some code stolen from entityreference_autocomplete module. Thank you!
 */
function ckentity_autocomplete($button_id, $curr_entity_type, $curr_entity_id, $string) {
  $matches = array();

  ctools_include('export');
  $button = ctools_export_crud_load('ckentity_button', check_plain($button_id));

  if (empty($button)) {
    die(t('The button is unknown or disabled, so this entity cannot be edited.'));
  }

  $data = unserialize($button->data);
  $entity_type = $data['entity_type'];

  // Get bundles if needed.
  $bundles = array();
  if (!empty($data['entity_config']['bundles'])) {
    // Remove the unselected bundles.
    $bundles = array_filter($data['entity_config']['bundles']);
  }

  $limit = 10;
  $entity_info = entity_get_info($entity_type);

  // Start an EntityFieldQuery for the matches.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  // Order by descending entity id, to show the newest first.
  $query->entityOrderBy('entity_id', 'DESC');
  // Filter by bundle if necessary.
  if (!empty($bundles) && !empty($entity_info['entity keys']['bundle'])) {
    $query->entityCondition('bundle', $bundles);
  }
  // Filter by label if a string was provided.
  if (!empty($string) && isset($entity_info['entity keys']['label'])) {

    $label_column = $entity_info['entity keys']['label'];
    $query->propertyCondition($label_column, '%' . db_like($string) . '%', 'LIKE');
  }

  // If curr_entity_type is the same as entity_type, then there is the risk of
  // an infinite loop if we allow an entity to be embedded on itself. So here we
  // filter out that entity from the results.
  if ($curr_entity_type == $entity_type) {

    if ($curr_entity_id != '*') {

      $query->entityCondition('entity_id', $curr_entity_id, '<>');
    }
  }

  // Set the maximum number of results returned.
  $query->range(0, $limit);
  $result = $query->execute();

  if (!empty($result[$entity_type])) {
    $entities = entity_load($entity_type, array_keys($result[$entity_type]));
    // $context will hold the metadata of entities found, so that modules
    // can alter the results in any way they want.
    $context = array(
      'button' => $button,
      'entities' => array(),
      'entity_type' => $entity_type,
    );

    // Iterate through all entities retrieved and process the data to return
    // it as expected by Drupal javascript.
    foreach ($entities as $entity_id => $entity) {
      if (entity_access('view', $entity_type, $entity)) {

        // Get the labels for the key and for the option.
        $label = entity_label($entity_type, $entity);

        $option = $label . ' - ' . $entity_id;
        $matches[$option] = check_plain($option);

        $context['entities'][$option] = $entity;
      }
    }
  }

  // Let other drupal modules alter the results.
  drupal_alter('ckentity_autocomplete_matches', $matches, $context);
  // Finally, output matches in json, as they're are expected by Drupal's ajax.
  drupal_json_output($matches);
}
