<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'ckentity_button',
  'access' => 'administer ckentity',
  'menu' => array(
    'menu prefix' => 'admin/config/content',
    'menu item' => 'ckentity',
    'menu title' => 'CKEntity buttons',
    'menu description' => 'Administer buttons for embedding entities into rich-text editors.',
  ),
  'title singular' => t('button'),
  'title plural' => t('buttons'),
  'title singular proper' => t('CKEntity button'),
  'title plural proper' => t('CKEntity buttons'),
  'form' => array(
    'settings' => 'ckentity_ctools_export_ui_form',
    'submit' => 'ckentity_ctools_export_ui_form_submit',
  ),
);

/**
 * Define the CKEntity button add/edit form.
 */
function ckentity_ctools_export_ui_form(&$form, &$form_state) {

  $button = $form_state['item'];
  $data = (empty($button->data)) ? FALSE : unserialize($button->data);

  $form['#attached']['css'][] = array(
    'type' => 'file',
    'data' => drupal_get_path('module', 'ckentity') .
      '/css/ckentity.admin.css',
  );

  $form['data'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['data']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Editor Label'),
    '#description' => t("The user-facing label for the toolbar button and pop-up dialog in the rich-text editor."),
    '#required' => TRUE,
    '#default_value' => ($data) ? $data['label'] : '',
  );

  $image_options = ckentity_get_icon_options();
  $form['data']['image'] = array(
    '#type' => (module_exists('jipi')) ? 'imagepicker' : 'select',
    '#title' => t('Icon'),
    '#description' => t('The icon that will be used in the CKEditor toolbar.'),
    '#options' => $image_options,
    '#default_value' => ($data) ? $data['image'] : NULL,
    '#attributes' => array(
      'class' => array(
        'ckentity-icon-select',
      ),
    ),
  );

  // Allow some custom help text on the button's dialog popup.
  $form['data']['help'] = array(
    '#type' => 'text_format',
    '#title' => t('Optional help text for the dialog popup'),
    '#default_value' => ($data) ? $data['help']['value'] : '',
    '#format' => ($data) ? $data['help']['format'] : NULL,
  );

  $form['data']['edit_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Edit Link'),
    '#description' => t('Display a link for users to edit the currently selected entity.'),
    '#default_value' => isset($data['edit_link']) ? $data['edit_link'] : FALSE,
  );

  $form['data']['create_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create Links'),
    '#description' => t('Display links for users to create new entities of the entity type/bundle.'),
    '#default_value' => isset($data['create_links']) ? $data['create_links'] : FALSE,
  );

  $form['data']['alignment'] = array(
    '#type' => 'checkbox',
    '#title' => t('Alignment'),
    '#description' => t('Allow the users to align the entities left, right, or center.'),
    '#default_value' => isset($data['alignment']) ? $data['alignment'] : FALSE,
  );

  $entity_info = entity_get_info();

  $entity_type_options = array();
  foreach ($entity_info as $entity_type => $info) {
    $entity_type_options[$entity_type] = $info['label'];
  }
  $form['data']['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity Type'),
    '#description' => t('The type of entity that will be embedded with this button.'),
    '#options' => $entity_type_options,
    '#default_value' => ($data) ? $data['entity_type'] : NULL,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'ckentity_button_form_entity_ajax',
      'wrapper' => 'ckentity-entity-config',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['data']['entity_config'] = array(
    '#prefix' => '<div id="ckentity-entity-config">',
    '#suffix' => '</div>',
  );

  // Get the entity type, either from saved settings or the current form values.
  $entity_type = FALSE;
  if (!empty($form_state['values']['data']['entity_type'])) {
    $entity_type = $form_state['values']['data']['entity_type'];
  }
  elseif (!empty($data['entity_type'])) {
    $entity_type = $data['entity_type'];
  }
  if (!empty($entity_type)) {

    // For entity selection, we will provide a default widget using autocomplete
    // by entity label, optionally limited by bundle.
    $entity_selection_options = array(
      'autocomplete' => t('Autocomplete (by label)'),
    );
    // Additionally if Entity Picker is enabled we'll allow any View to be used
    // as the entity selection widget, as long as it has the same base table.
    if (module_exists('entity_picker')) {

      $all_views = views_get_all_views();
      $base_table = entity_get_info($entity_type)['base table'];
      foreach ($all_views as $view) {

        if ($view->base_table == $base_table) {
          $label = (empty($view->human_name)) ? $view->name : $view->human_name;
          foreach ($view->display as $display_id => $display) {
            $entity_selection_options[$view->name . '|' . $display_id] = 'View: ' . $label . ' - ' . $display->display_title;
          }
        }
      }
    }

    $form['data']['entity_config']['entity_selection'] = array(
      '#type' => 'select',
      '#options' => $entity_selection_options,
      '#required' => TRUE,
      '#title' => t('Entity Selection'),
      '#description' => t('Choose the method of entity selection - either an autocomplete, or a Views view if available.'),
      '#default_value' => ($data) ? $data['entity_config']['entity_selection'] : NULL,
    );

    if (!empty($entity_info[$entity_type]['bundles']) && count($entity_info[$entity_type]['bundles']) > 1) {

      $bundle_options = array();
      foreach ($entity_info[$entity_type]['bundles'] as $bundle => $info) {
        $bundle_options[$bundle] = $info['label'];
      }
      $form['data']['entity_config']['bundles'] = array(
        '#type' => 'checkboxes',
        '#options' => $bundle_options,
        '#title' => t('Bundles'),
        '#description' => t('Specify bundles for the selected entity type. This will control which "Create links" are displayed, if that option is enabled. Also if the Entity Selection is "Autocomplete", the autocomplete suggestions will be filtered to only include these bundles.'),
        '#default_value' => ($data) ? $data['entity_config']['bundles'] : NULL,
      );
    }

    // Allow selection of display mode and settings.
    $formatters = ckentity_get_entity_field_formatters($entity_type);
    if (!empty($formatters)) {

      $formatter_options = array();
      foreach ($formatters as $formatter => $info) {
        $formatter_options[$info['module'] . ':' . $formatter] = $info['label'];
      }

      // First display the existing display modes.
      $form['data']['entity_config']['formatters'] = array(
        '#type' => 'fieldset',
        '#title' => t('Formatter Options'),
        '#description' => t('Configure the options that will be available as a drop-down in the rich-text editor for displaying the entities.'),
        '#prefix' => '<div id="formatters-fieldset-wrapper">',
        '#suffix' => '</div>',
      );

      // Figure out how many saved formatter configurations there are.
      if (empty($form_state['entity_config']['num_formatters'])) {

        if (empty($data['entity_config']['formatters'])) {

          $form_state['entity_config']['num_formatters'] = 1;
        }
        else {

          $form_state['entity_config']['num_formatters']
            = count($data['entity_config']['formatters']);
        }
      }
      for ($i = 0; $i < $form_state['entity_config']['num_formatters']; $i++) {

        $form['data']['entity_config']['formatters'][$i] = array(
          '#type' => 'fieldset',
          '#attributes' => array(
            'class' => array('ckentity-formatter-fieldset'),
          ),
        );

        $form['data']['entity_config']['formatters'][$i]['label'] = array(
          '#title' => t('Label in drop-down'),
          '#description' => t('Optional user-facing label for this option.'),
          '#type' => 'textfield',
          '#size' => 30,
          '#default_value' => ($data) ? $data['entity_config']['formatters'][$i]['label'] : NULL,
          '#prefix' => '<div class="ckentity-formatter-label ckentity-formatter-item">',
          '#suffix' => '</div>',
        );

        $form['data']['entity_config']['formatters'][$i]['formatter'] = array(
          '#required' => TRUE,
          '#title' => t('Formatter'),
          '#description' => t('Display formatter for this option.'),
          '#type' => 'select',
          '#options' => $formatter_options,
          '#default_value' => ($data) ? $data['entity_config']['formatters'][$i]['formatter'] : NULL,
          // Pass the index to the ajax callback.
          '#myindex' => $i,
          '#ajax' => array(
            'callback' => 'ckentity_button_form_formatter_ajax',
            'wrapper' => 'formatter-config-' . $i,
            'method' => 'replace',
            'effect' => 'fade',
          ),
          '#prefix' => '<div class="ckentity-formatter-type ckentity-formatter-item">',
          '#suffix' => '</div>',
        );

        $form['data']['entity_config']['formatters'][$i]['config'] = array(
          '#prefix' => '<div id="formatter-config-' . $i . '" class="ckentity-formatter-item">',
          '#suffix' => '</div>',
        );

        // Get the formatter, either from saved settings or from form input.
        $formatter = FALSE;
        if (!empty($form_state['values']['data']['entity_config']['formatters'][$i]['formatter'])) {
          $formatter = $form_state['values']['data']['entity_config']['formatters'][$i]['formatter'];
        }
        elseif (!empty($data['entity_config']['formatters'][$i]['formatter'])) {
          $formatter = $data['entity_config']['formatters'][$i]['formatter'];
        }
        // Parse the module from the formatter.
        $formatter_parts = explode(':', $formatter);
        $formatter_module = $formatter_parts[0];
        $formatter = $formatter_parts[1];
        if (!empty($formatter) && !empty($formatters[$formatter]['settings'])) {

          // Hack our way to the settings form for this formatter.
          $settings_form_func = $formatter_module . '_field_formatter_settings_form';
          if (function_exists($settings_form_func)) {

            // Dummy parameters when needed, others from helper functions.
            $field = ckentity_field_info_field($entity_type, $formatter_module);
            // Pass our existing settings into the $instance parameter so that
            // the form will have its default values.
            $existing_settings = ($data) ? $data['entity_config']['formatters'][$i]['config'] : NULL;
            $instance = ckentity_field_info_instance($formatter, $existing_settings);
            // Dummy values for view mode, form, and form state.
            $view_mode = 'default';
            $dummy_form = array();
            $dummy_form_state = array();
            // Finally try to build the array.
            $settings_form = $settings_form_func($field, $instance, $view_mode, $dummy_form, $dummy_form_state);

            if (!empty($settings_form)) {
              // Stick the settings form onto our form.
              $form['data']['entity_config']['formatters'][$i]['config'] += $settings_form;
            }
          }
        }
      }
      $form['data']['entity_config']['formatters']['add_formatter'] = array(
        '#type' => 'submit',
        '#value' => t('Add a formatter option'),
        '#submit' => array('ckentity_add_formatter'),
        '#attributes' => array(
          'class' => array('ckentity-formatter-button'),
        ),
        '#ajax' => array(
          'callback' => 'ckentity_add_formatter_ajax',
          'wrapper' => 'formatters-fieldset-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
        ),
      );
      if ($form_state['entity_config']['num_formatters'] > 1) {

        $form['data']['entity_config']['formatters']['remove_formatter'] = array(
          '#type' => 'submit',
          '#value' => t('Remove a formatter option'),
          '#submit' => array('ckentity_remove_formatter'),
          '#attributes' => array(
            'class' => array('ckentity-formatter-button'),
          ),
          '#ajax' => array(
            'callback' => 'ckentity_add_formatter_ajax',
            'wrapper' => 'formatters-fieldset-wrapper',
            'method' => 'replace',
            'effect' => 'fade',
          ),
        );
      }
    }
  }
}

/**
 * Define the button add/edit form submit callback.
 */
function ckentity_ctools_export_ui_form_submit(&$form, &$form_state) {

  form_state_values_clean($form_state);
  $form_state['values']['data'] = serialize($form_state['values']['data']);
}

/**
 * Ajax callback for the entity-type-specific form fields.
 */
function ckentity_button_form_entity_ajax($form, $form_state) {

  return $form['data']['entity_config'];
}

/**
 * Ajax callback for the formatter-specific form fields.
 */
function ckentity_button_form_formatter_ajax($form, $form_state) {

  $index = $form_state['triggering_element']['#myindex'];
  return $form['data']['entity_config']['formatters'][$index]['config'];
}

/**
 * Submit callback for adding a formatter.
 */
function ckentity_add_formatter($form, &$form_state) {

  $form_state['entity_config']['num_formatters']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit callback for removing a formatter.
 */
function ckentity_remove_formatter($form, &$form_state) {

  if ($form_state['entity_config']['num_formatters'] > 1) {
    $form_state['entity_config']['num_formatters']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback for adding/removing a formatter.
 */
function ckentity_add_formatter_ajax($form, $form_state) {

  return $form['data']['entity_config']['formatters'];
}
