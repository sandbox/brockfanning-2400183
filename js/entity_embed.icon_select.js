(function ($) {

    Drupal.behaviors.ckentityIconSelect = {

        attach: function (context, settings) {

            $('.ckentity-icon-select option').each(function (index) {
                $(this).attr('data-img-src', $(this).val());
            });

            $('.ckentity-icon-select').imagepicker();
        }
    };
}(jQuery));