<?php
/**
 * @file
 * Document hooks for CKEntity module.
 */

/**
 * Get a field_info_field array suitable for hacking into field formatter hooks.
 *
 * @param string $entity_type
 *   The entity type of the entity that will be embedded using a field
 *   formatter.
 *
 * @param string $module
 *   The module that implements the field formatter that will be used to embed
 *   the entity.
 *
 * @return array
 *   An array similar to what field_info_field() would return, but containing
 *   only what is needed to fake our way through field formatter hooks. The
 *   results of this hook will be passed as the $field parameter in both
 *   hook_field_formatter_view() and hook_field_formatter_settings_form().
 */
function hook_ckentity_field_info_field($entity_type, $module) {

  if ($module == 'mymodule') {
    return array(
      // My field formatters need the entity type set as "foo", and also expect
      // an unimportant "bar" array.
      'foo' => $entity_type,
      'bar' => array(),
    );
  }
}

/**
 * Get a field_get_items array suitable for hacking into field formatter hooks.
 *
 * @param string $entity_type
 *   The entity type of the entity that will be embedded using a field
 *   formatter.
 *
 * @param string $entity_id
 *   The entity id of the entity that will be embedded using a field formatter.
 *
 * @param string $module
 *   The module that implements the field formatter that will be used to embed
 *   the entity.
 *
 * @return array
 *   An array similar to what field_get_items() would return, but containing
 *   only what is needed to fake our way through field formatter hooks. The
 *   results of this hook will be passed as the $items parameter in
 *   hook_field_formatter_view().
 */
function hook_ckentity_field_get_items($entity_type, $entity_id, $module) {

  if ($module == 'mymodule') {

    $items[] = array(
      // My field formatters need the items to be set as arrays with the fully
      // loaded entity assigned to "foo", and the entity_type assigned to "bar".
      'foo' => entity_load_single($entity_type, $entity_id),
      'bar' => $entity_id,
    );
    return $items;
  }
}

/**
 * Get all the field types that display entities, per entity type.
 *
 * @param string $entity_type
 *   The entity type of the entity that will be embedded using a field
 *   formatter.
 *
 * @return array
 *   An array of field types that display entities of the passed $entity_type.
 */
function hook_ckentity_field_types($entity_type) {

  if ('my_entity_type' == $entity_type) {

    return array(
      'field_type_that_references_my_entity_type',
    );
  }
}

/**
 * Declare icons for use on rich-text editor buttons. These should be 16x16.
 *
 * @return array
 *   An array of icon paths, keyed by a unique identifier.
 */
function hook_ckentity_icons() {

  $path = base_path() . drupal_get_path('module', 'mymodule') . '/myicons/';
  return array(
    'foo' => $path . 'bar.png',
    'another' => $path . 'baz.png',
  );
}

/**
 * Alter the matches that are returned from the CKEditor dialog's autocomplete.
 *
 * @param array $matches
 *   Associative array of options for an autocomplete select list.
 *
 * @param array $context
 *   Associate array of data containing:
 *     button       The button object that is being used to find an entity.
 *     entities     Array of entities keyed in the same way as the $match param.
 *     entity_type  The type of the entities listed.
 */
function hook_ckentity_autocomplete_matches_alter(&$matches, $context) {

  if ($context['button']->name == 'my_button_name') {

    foreach ($context['entities'] as $key => $entity) {

      // I want thumbnails too, instead of just a label.
      $entity_type = $context['entity_type'];

      // I happen to know that these entities might have a "field_image"
      // field, so check for that.
      $image = field_get_items($entity_type, $entity, 'field_image');
      if (!empty($image[0])) {
        // I found it, so stick it onto the $matches value.
        $matches[$key] .= theme('image_style', array(
          'path' => $image[0]['uri'],
          'style_name' => 'thumbnail',
        ));
      }
    }
  }
}

/**
 * Declare that your module provides default buttons.
 *
 * Your module may already implement this hook for other CTools plugin types.
 * If so, copy the body of this function into the existing hook.
 */
function hook_ctools_plugin_api($owner, $api) {

  if ($owner == 'ckentity' && $api == 'ckentity_button') {

    return array('version' => 1);
  }
}

/**
 * Provide default embed buttons.
 *
 * This is the hook where you can copy/paste the exports of your buttons,
 * to keep their configuration in code.
 */
function hook_ckentity_default_button() {

  $buttons = array();

  $button = new stdClass();
  $button->disabled = FALSE; /* Edit this to true to make a default button disabled initially */
  $button->api_version = 1;
  $button->name = 'article_button';
  $button->admin_title = 'Article Button';
  $button->data = 'a:4:{s:5:"label";s:13:"Article Embed";s:5:"image";s:14:"ckentity_a";s:11:"entity_type";s:4:"node";s:13:"entity_config";a:2:{s:7:"bundles";a:2:{s:7:"article";s:7:"article";s:8:"settings";i:0;}s:10:"formatters";a:2:{i:0;a:3:{s:5:"label";s:9:"Link only";s:9:"formatter";s:37:"entityreference:entityreference_label";s:6:"config";a:1:{s:4:"link";i:1;}}i:1;a:3:{s:5:"label";s:6:"Teaser";s:9:"formatter";s:43:"entityreference:entityreference_entity_view";s:6:"config";a:2:{s:9:"view_mode";s:6:"teaser";s:5:"links";i:0;}}}}}';

  $buttons['article_button'] = $button;

  return $buttons;
}