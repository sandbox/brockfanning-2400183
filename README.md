# CKEntity

This module was originally a D7 port of the [Entity Embed module for D8](http://drupal.org/project/entity_embed) but is now a separate project.

## Installation

Download and install the module as normal.

This module requires the CKEditor rich text editor, including the Widgets plugin. This is NOT standard, FYI, but here is a link to [build your own CKEditor, starting with standard + Widgets](http://ckeditor.com/builder/a9fb5287b16c2507e2e984e5f601ddb5).

To integrate with Drupal, you need to use either [WYSIWYG](http://drupal.org/project/wysiwyg) module (dev version!) or [CKEditor](http://drupal.org/project/ckeditor) module. So far this has only been tested with the dev version of WYSIWYG.

Optional: To use a Views-based entity selection browser instead of the standard autocomplete, you need to have [Entity Picker](https://www.drupal.org/sandbox/brockfanning/2410025) installed.

Optional: For a more user-friendly icon-picker, download and install: [Jipi](http://drupal.org/project/jipi)

## Configuration

Go to /admin/config/content/ckentity

Click "Add button" to create a new button.

In the button configuration page, choose an entity type, and an entity selection mode. If you choose "Autocomplete" as the entity selection mode, you can also (optionally) limit the autocomplete to certain bundles. If you have Entity Picker enabled you can alternatively use a View as the selection mode. Finally, create one or more "Formatter options". These are display options that content editors will see in the CKEditor dialog.

After saving the button, go to your CKeditor configuration page (for WYSIWYG module, you can get there from /admin/config/content/wysiwyg). The button should show up in the list of available buttons. Check it and save.

In your text format settings (/admin/config/content/formats) check "Display embedded entities" and save.

## Usage

In CKEditor, position the cursor and click the toolbar button. Select an entity by typing in the title and selecting the autocomplete results (or if you choose the Views-based entity selection, click "Browse" and select the entity from the popup.) Choose a "formatter" if necessary, and an alignment option, as needed. Click OK.

To edit an existing entity, either double-click on it or right click and choose "Edit Entity" from the context menu.

## Todo

* Find/make non-crappy icons
* Optimize with some caching
* What can we do about security/access
* Provide some default buttons
* General testing