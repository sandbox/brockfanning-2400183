(function ($, Drupal, CKEDITOR) {
  "use strict";

  // Extend the DTD to avoid issues when switching between plain/rich text.
  CKEDITOR.dtd['drupal-entity'] = CKEDITOR.dtd;
  CKEDITOR.dtd.$blockLimit['drupal-entity'] = 1;
  CKEDITOR.dtd.$inline['drupal-entity'] = 1;
  CKEDITOR.dtd.$nonEditable['drupal-entity'] = 1;

  CKEDITOR.plugins.add('ckentity', {
    // This plugin requires the Widgets System defined in the 'widget' plugin.
    requires: 'widget',
    icons: 'ckentity',
    init: function (editor) {

      // Add our data-align CSS.
      editor.addContentsCss(this.path + 'styles/ckentity.css' );

      CKEDITOR.dialog.add('ckentityDialog', function (editor) {

        return {
          title: Drupal.t('Drupal Entity'),
          contents: [{
            id: 'info',
            elements: [
              {
                type: 'html',
                html: '',
                setup: function (widget) {
                  this.getElement().setHtml(widget.data.ckentityButton.help);
                }
              },
              {
                type: 'text',
                label: Drupal.t('Selection'),
                id: 'entityId',
                validate: CKEDITOR.dialog.validate.notEmpty(Drupal.t('Selection cannot be empty.')),
                setup: function (widget) {
                  // Show saved value.
                  this.setValue(widget.element.getAttribute('data-entity-id'));

                  // Set up autocomplete if not using a browser.
                  if (!widget.data.ckentityButton.browser) {
                    var $element = $(this.getInputElement().$);
                    $element.addClass('form-autocomplete');
                    $element.attr('autocomplete', 'OFF');
                    var currentEntityId = '*';
                    var currentEntityType = '*';
                    if (typeof Drupal.settings.ckentity.currentEntityType !== 'undefined') {

                      currentEntityType = Drupal.settings.ckentity.currentEntityType;
                      currentEntityId = Drupal.settings.ckentity.currentEntityId;
                    }
                    var autocompletePath = [
                      Drupal.settings.ckentity.autocompletePathBase,
                      widget.data.ckentityButton.name,
                      currentEntityType,
                      currentEntityId
                    ];
                    var acdb = new Drupal.ACDB(autocompletePath.join('/'));
                    new Drupal.jsAC($element, acdb);
                  }
                  // Otherwise we are using an entity_picker.module browser.
                  else {
                    var self = this;
                    var thisElement = this.getElement().$;
                    var loading = $('.ckentity-wait');
                    if (!loading.length) {
                      loading = $('<span class="ckentity-wait">' + Drupal.t('Please wait') + '...</span>');
                      $(loading).hide();
                      $(thisElement).after(loading);
                    }
                    var viewParts = widget.data.ckentityButton.browserInfo.split('|');

                    // Expect the user to  click on the input to open the browser.
                    $(thisElement).click(function () {
                      $(thisElement).trigger('entityPickerOpen', viewParts);
                      $(loading).show();
                    });
                    // Listen for the browser to finish loading.
                    $(thisElement).on('entityPickerLoaded', function (e) {
                      $(loading).hide();
                    });
                    // Listen for the browser to be submitted.
                    $(thisElement).on('entityPickerSubmit', function (e, data) {
                      if (typeof data.id !== 'undefined' && typeof data.label !== 'undefined') {
                        self.setValue(data.label + ' - ' + data.id);
                      }
                    });
                    // Also disable this input, since we don't want it editable.
                    $(thisElement).find('input').attr('readonly', 'readonly');
                    $(thisElement).find('input').attr('placeholder', Drupal.t('Click here to make a selection.'));
                  }
                }
              },
              {
                type: 'html',
                html: '',
                setup: function (widget) {
                  var thisElement = this.getElement().$;
                  this.getElement().setHtml('');

                  // Show an edit link if needed.
                  if (widget.data.ckentityButton.editLink) {
                    var dialog = this.getDialog();
                    $('<a>' + Drupal.t('Edit') + '</a>')
                      .attr('href', '#')
                      .addClass('cke_dialog_ui_button')
                      .appendTo($(thisElement))
                      .click(function(e) {
                        e.preventDefault();
                        var current = dialog.getValueOf('info', 'entityId');
                        if (current.length) {
                          var idStart = current.lastIndexOf('-');
                          if (idStart !== -1) {
                            var id = current.substr(idStart + 1).trim();
                            var editPath = widget.data.ckentityButton.editLink.replace('[id]', id);

                            window.open(Drupal.settings.basePath + editPath);
                          }
                        }
                        else {
                          alert(Drupal.t('In order to edit an entity, you must first make a selection above.'));
                        }
                      })
                      .after('&nbsp;');
                  }

                  // Show creation links if needed.
                  if (widget.data.ckentityButton.createLinks) {
                    for (var path in widget.data.ckentityButton.createLinks) {
                      $('<a>' + widget.data.ckentityButton.createLinks[path] + '</a>')
                        .attr('href', Drupal.settings.basePath + path)
                        .attr('target', '_blank')
                        .addClass('cke_dialog_ui_button')
                        .appendTo($(thisElement))
                        .after('&nbsp;');
                    }
                  }

                  if (widget.data.ckentityButton.createLinks ||
                      widget.data.ckentityButton.editLink) {
                    this.getElement().show();
                  }
                  else {
                    this.getElement().hide();
                  }
                }
              },
              {
                type: 'select',
                id: 'ckentityDisplay',
                label: Drupal.t('Formatter'),
                items: [['Placeholder', 'placeholder']],
                validate: CKEDITOR.dialog.validate.notEmpty(Drupal.t('Formatter cannot be empty.')),
                setup: function (widget) {

                  // Check the current value on the element so
                  // we can populate this select.
                  var currentVal = widget.element.getAttribute('data-entity-embed-display');

                  // Set the items dynamically.
                  this.clear();
                  var numOptions = 0;
                  for (var key in widget.data.ckentityButton.formatters) {
                    numOptions += 1;
                    var formatterInfo = widget.data.ckentityButton.formatters[key];
                    this.add(formatterInfo.label, key);
                    if (currentVal === formatterInfo.formatter) {
                      this.setValue(key);
                    }
                  }

                  // Hide this if there is only one option,
                  // to simplify the UI.
                  if (numOptions > 1) {
                    this.getElement().show();
                  }
                  else {
                    this.getElement().hide();
                  }

                }
              },
              {
                id: 'align',
                type: 'select',
                label: 'Align',
                items: [
                  [editor.lang.common.notSet, ''],
                  [editor.lang.common.alignLeft, 'left'],
                  [editor.lang.common.alignRight, 'right'],
                  [editor.lang.common.alignCenter, 'center']
                ],
                setup: function (widget) {
                  if (widget.data.ckentityButton.alignment) {
                    this.setValue(widget.element.getAttribute('data-align'));
                    this.getElement().show();
                  }
                  else {
                    this.getElement().hide();
                  }
                }
              }
            ]
          }],
          onShow: function () {

            var widget = this.widget;

            // If an entity was embedded and then the CKEntity button
            // was disabled or deleted from the WYSIWYG, we won't
            // know how configure the dialog, so we disallow any
            // editing of this widget.
            if (!widget.buttonIsKnown()) {

              this.hide();
              alert(Drupal.t('This entity was embedded with a button which has been disabled or deleted, so it cannot be edited.'));
            }
            else {
              // Customize this dialog.
              this.parts.title.setHtml(widget.data.ckentityButton.label);
            }
          },
          onOk: function () {

            var widget = this.widget;

            // Set values based on the user choices.
            widget.element.setAttribute('data-entity-id',
              this.getValueOf('info', 'entityId'));
            var formatterKey = this.getValueOf('info', 'ckentityDisplay');
            widget.element.setAttribute('data-entity-embed-display',
              widget.data.ckentityButton.formatters[formatterKey].formatter);
            widget.element.setAttribute('data-entity-embed-settings',
              JSON.stringify(widget.data.ckentityButton.formatters[formatterKey].config));
            widget.element.setAttribute('data-align',
              this.getValueOf('info', 'align'));

            // Set some values that we don't actually need to be
            // user-configurable. These are just passed along from
            // the Drupal settings for this button.
            widget.element.setAttribute('data-entity-type', widget.data.ckentityButton.entityType);
            widget.element.setAttribute('data-embed-button', widget.data.ckentityButton.name);
            widget.updateEmbeddedEntity();
          }
        };
      });

      // Register the toolbar buttons for the CKEditor editor instance.
      for (var key in Drupal.settings.ckentity.buttons) {
        var button = Drupal.settings.ckentity.buttons[key];
        editor.ui.addButton(button.name, {
          label: button.label,
          icon: button.image,
          data: button,
          click: function (editor) {

            if (!editor.focusManager.hasFocus) {

              alert(Drupal.t('Please select an existing entity, or place the cursor where you would like to insert a new entity.'));
              return;
            }

            // No other way to pass data to widget commands?
            editor.ckentityLastButtonPressed = this.data;
            editor.execCommand('ckentity');
          }
        });
      }

      editor.widgets.add('ckentity', {

        allowedContent: 'drupal-entity[data-entity-type,data-entity-id,data-entity-embed-display,data-entity-embed-settings,data-embed-button,data-align]',
        dialog: 'ckentityDialog',
        template: '<drupal-entity></drupal-entity>',
        requiredContent: 'drupal-entity[data-entity-type,data-entity-id,data-entity-embed-display]',

        init: function () {

          // We need to record which button was used to embed the entity.
          // First check a temporary variable set in the button click event.
          if (typeof editor.ckentityLastButtonPressed !== 'undefined') {
            this.setData('ckentityButton', editor.ckentityLastButtonPressed);
            delete editor.ckentityLastButtonPressed;
          }
          // Otherwise use whatever is already set in the element.
          else if (this.element.hasAttribute('data-embed-button')) {
            var buttonName = this.element.getAttribute('data-embed-button');
            if (typeof Drupal.settings.ckentity.buttons[buttonName] !== 'undefined') {
              this.setData('ckentityButton',
                Drupal.settings.ckentity.buttons[buttonName]);
            }
          }
          // Otherwise fallback to a string to indicate an unknown button.
          if (typeof this.data.ckentityButton === 'undefined') {
            this.setData('ckentityButton', null);
          }

          // Pass the reference to this widget to the dialog.
          this.on('dialog', function (event) {
            event.data.widget = this;
          }, this);

          this.updateEmbeddedEntity();
        },
        upcast: function (element) {
          var attributes = element.attributes;
          if (attributes['data-entity-type'] === undefined || attributes['data-entity-id'] === undefined) {
            return;
          }
          return element;
        },
        downcast: function (element) {
          // Only keep the wrapping element.
          element.setHtml('');
          // Remove the auto-generated ID.
          delete element.attributes.id;
          return element;
        },
        // Helper function to run the ajax call and populate the custom
        // <drupal-entity> tag with HTML.
        updateEmbeddedEntity: function () {

          var element = this.element;

          // If required attributes are not present, abort.
          if (!element.hasAttribute('data-entity-id')) {

            return;
          }

          // Make sure it has an id, for the ajax framework.
          var $element = $(element.$);
          var base = $element.attr('id');
          if (typeof base === 'undefined') {
            base = generateEmbedId();
            $element.attr('id', base);
          }

          // Clear the element if it already has content.
          $element.html('');

          Drupal.ajax[base] = new Drupal.ajax(base, $element, {
            url: Drupal.settings.ckentity.previewPathBase + '?' + $.param({
              value: element.getOuterHtml()
            }),
            progress: {type: 'none'},
            // Use a custom event to trigger the call.
            event: 'ckentityDummyEvent'
          });
          // Trigger the call manually.
          $element.trigger('ckentityDummyEvent');
        },
        // Helper function to tell if the button is known.
        buttonIsKnown: function () {
          var knownButtons = Drupal.settings.ckentity.buttons;
          var buttonName = this.data.ckentityButton.name;
          return typeof knownButtons[buttonName] !== 'undefined';
        }
      });

      // Register context menu option for editing widget.
      if (editor.contextMenu) {
        editor.addMenuGroup('ckentity');
        editor.addMenuItem('ckentity', {
          label: Drupal.t('Edit Entity'),
          icon: this.path + 'icons/drupalentity.png',
          command: 'ckentity',
          group: 'ckentity'
        });

        editor.contextMenu.addListener(function (element) {
          if (isEntityWidget(editor, element)) {
            return {ckentity: CKEDITOR.TRISTATE_OFF};
          }
        });
      }
    }
  });

  /**
   * Returns whether or not the given element is a ckentity widget.
   *
   * @param {CKEDITOR.editor} editor
   * @param {CKEDITOR.htmlParser.element} element
   */
  function isEntityWidget(editor, element) {
    var widget = editor.widgets.getByElement(element, true);
    return widget && widget.name === 'ckentity';
  }

  /**
   * Generates unique HTML IDs for the widgets.
   *
   * @returns {string}
   */
  function generateEmbedId() {
    if (typeof generateEmbedId.counter == 'undefined') {
      generateEmbedId.counter = 0;
    }
    return 'ckentity-' + generateEmbedId.counter++;
  }

  /**
   * Ajax 'ckentityInsert' command: insert the rendered entity.
   *
   * The regular Drupal.ajax.commands.insert() command cannot target elements
   * within iframes. This is a skimmed down equivalent that works whether the
   * CKEditor is in iframe or divarea mode.
   */
  Drupal.ajax.prototype.commands.ckentityInsert = function (ajax, response, status) {

    var $target = ajax.element;
    // No need to detach behaviors here, the widget is created fresh each time.
    $target.html(response.html);

    runEmbedBehaviors('attach', $target.get(0), response.settings || ajax.settings);
  };

  /**
   * Attaches or detaches behaviors, except the ones we do not want.
   *
   * @param {string} action
   *   Either 'attach' or 'detach'.
   * @param context
   *   The context argument for Drupal.attachBehaviors()/detachBehaviors().
   * @param settings
   *   The settings argument for Drupal.attachBehaviors()/detachBehaviors().
   */
  function runEmbedBehaviors(action, context, settings) {
    // Do not run the following behaviors:
    // - Drupal.behaviors.editor, to avoid CK inception.
    // - Drupal.behaviors.contextual, to keep contextual links hidden.
    var stashed = {};
    $.each(['editor', 'contextual'], function (i, behavior) {
      stashed[behavior] = Drupal.behaviors[behavior];
      delete Drupal.behaviors[behavior];
    });
    // Run the remaining behaviors.
    (action == 'attach' ? Drupal.attachBehaviors : Drupal.detachBehaviors)(context, settings);
    // Put the stashed behaviors back in.
    $.extend(Drupal.behaviors, stashed);
  }

}(jQuery, Drupal, CKEDITOR));